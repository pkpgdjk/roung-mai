const mongoose = require("mongoose");
let mongooseHidden = require('mongoose-hidden')()
let autopopulate = require('mongoose-autopopulate')

const Customer = mongoose.Schema({
    cardNumber: {
        type: String,
        required: true,
        trim: true,
        minlength: 16,
        maxlength: 16
    },
    cardName: {
        type: String,
        required: true,
        trim: true,
    },
    expireDate: {
        type: String,
        required: true,
        trim: true,
        hide: true
    },
    cvv: {
        type: String,
        required: true,
        trim: true,
        minlength: 3,
        maxlength: 3,
        hide: true
    },
},{timestamps: true});

Customer.plugin(autopopulate);
Customer.plugin(mongooseHidden);


module.exports = mongoose.model("Customer",Customer)
