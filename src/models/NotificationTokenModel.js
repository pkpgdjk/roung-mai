const mongoose = require("mongoose");
const NotificationToken = mongoose.Schema({
    cusId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    notificationToken:{
        type: String,
        required: true
    }
},{timestamps: true});


module.exports = mongoose.model("NotificationToken", NotificationToken);
