const mongoose = require("mongoose");
const {ORDER_STATUS_TYPE}  = require("../constants");

const Order = mongoose.Schema({
    seller:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    menu: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Menu',
        required: true
    },
    message: {
        type: String,
        trim: true
    },
    price: {
        type: Number,
        required: true,
    },
    customer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    status: {
        type: String,
        enum: [ORDER_STATUS_TYPE.FINISH, ORDER_STATUS_TYPE.COOKING, ORDER_STATUS_TYPE.CANCELED],
        trim: true,
        required: true
    },
},{timestamps: true});


module.exports = mongoose.model("Order", Order);
