const mongoose = require("mongoose");
const Payment = mongoose.Schema({
    menuId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Menu',
        required: true
    },
    price: {
        type: Number,
        required: true,
    },
    cusId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Customer',
        required: true
    },
    storeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Seller',
        required: true
    },
},{timestamps: true});


module.exports = mongoose.model("Payment", Payment);
