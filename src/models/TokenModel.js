const mongoose = require("mongoose");
const bcrypt = require('bcrypt')

const TokenModel = mongoose.Schema({
    token: {
        type: String,
        required: true,
    }
},{timestamps: true})

TokenModel.methods.compare = function(token) {
    if (this.token === token){
        return true;
    }
};
module.exports = mongoose.model("Token",TokenModel)
