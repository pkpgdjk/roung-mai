const mongoose = require("mongoose");
const bcrypt = require('bcrypt')
const mongooseHidden = require('mongoose-hidden')()
const autopopulate = require('mongoose-autopopulate')

const Token = require("../models/TokenModel");
const Seller = require("../models/SellerModel");
const Customer = require("../models/CustomerModel");
const {BCRYPT_SALT} = require("../../config");
const {USER_TYPE} = require("../constants");
const {JWT_KEY} = require('../../config');
const jwt = require('jsonwebtoken')
const isEmpty = require('lodash/isEmpty')


const User = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    imageFile: {
        type: String,
    },
    userType: {
        type: String,
        enum: [USER_TYPE.CUSTOMER, USER_TYPE.SELLER],
        required: true
    },
    customer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Customer',
        autopopulate: true,
    },
    seller: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Seller',
        autopopulate: true,
    },
},{timestamps: true});

// User.plugin(mongooseHidden);
User.plugin(autopopulate);


User.pre('save', function (next) {
    let user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(BCRYPT_SALT, function (err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

User.methods.toJSON = function () {
    var obj = this.toObject();
    delete obj.password;
    return obj;
}

User.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

User.methods.getToken = async function () {
    let user = this.toObject();
    delete user.imageFile;
    delete user.password;
    delete user.updatedAt;
    delete user.createdAt;
    if (user.customer){
        delete user.customer.updatedAt;
        delete user.customer.updatedAt;
    }else if (user.seller){
        delete user.seller.updatedAt;
        delete user.seller.createdAt;
        delete user.seller.imageFile;

    }
    const token = jwt.sign({user}, JWT_KEY);
    await (new Token({token})).save();
    return token;
};


module.exports = mongoose.model("User", User);
