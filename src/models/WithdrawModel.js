const mongoose = require("mongoose");
const {WITHDRAW_TYPE} = require("../constants")

const Withdraw = mongoose.Schema({
    storeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    amount: {
        type: Number,
        required: true,
    },
    method: {
        type: String,
        enum: [WITHDRAW_TYPE.BANK, WITHDRAW_TYPE.PROMPTPAY],
        required: true,
    },
    bankType: {
        type: String,
        trim: true,
    },
    bankNumber: {
        type: String,
        trim: true
    },
    promptpayAccount: {
        type: String,
        trim: true
    },
},{timestamps: true});


module.exports = mongoose.model("Withdraw",Withdraw)
