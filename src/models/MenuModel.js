const mongoose = require("mongoose");

const Menu = mongoose.Schema({
    menuName: {
        type: String,
        required: true,
        trim: true,
    },
    description: {
        type: String,
        trim: true,
    },
    price: {
        type: Number,
        required: true,
    },
    imageFile: {
        type: String,
        trim: true,
    },
    seller: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
},{timestamps: true});


module.exports = mongoose.model("Menu",Menu)
