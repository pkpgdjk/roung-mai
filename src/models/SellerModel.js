const mongoose = require("mongoose");
let autopopulate = require('mongoose-autopopulate')


const Seller = mongoose.Schema({
    storeName: {
        type: String,
        required: true,
    },
    idCard: {
        type: String,
        maxlength: 13,
        minLength: 13,
        required: true,
    },
    money: {
        type: Number,
        required: true,
    },
    description: {
        type: String,
    },
    imageFile: {
        type: String,
    },
},{timestamps: true});

Seller.plugin(autopopulate);


module.exports = mongoose.model("Seller", Seller);
