var Token = require('../models/TokenModel');
var {JWT_KEY} = require('../../config');
const {USER_TYPE} = require('../constants');
var jwt = require('jsonwebtoken');

const paginationMiddleware = async (req, res, next) => {
    if (!req.query.page)
        req.query.page = 1;
    else
        req.query.page = parseInt(req.query.page) || 1;

    if (!req.query.perPage)
        req.query.perPage = 20;
    else
        req.query.perPage = parseInt(req.query.perPage) || 20;


    next();
};

module.exports = paginationMiddleware
