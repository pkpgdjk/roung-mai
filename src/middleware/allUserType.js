var Token = require('../models/TokenModel');
var {JWT_KEY} = require('../../config');
const {USER_TYPE} = require('../constants');
var jwt = require('jsonwebtoken');

const allUserType = async (req, res, next) => {
    const t = req.headers["authorization"];
    let token = "";
    if (t && t.split(" ")[1]) {
        token = t.split(" ")[1]
    } else {
        res.status(401).json({message: "คุณยังไม่ได้เข้าสู้ระบบ"});
        return;
    }
    try {
        console.log(token);
        const match = await Token.findOne({token});
        if (match) {
            let decoded = jwt.verify(token, JWT_KEY);
            if (!decoded.user.userType) {
                res.status(401).json({message: "คุณไม่ได้รับอณุญาติให้เข้าถึง"});
                return;
            }

            req['auth_user'] = decoded.user;
            req['token'] = token;
            next()

        } else {
            res.status(401).json({message: "คุณยังไม่ได้เข้าสู้ระบบ"});
        }
    } catch (err) {
        res.status(401).json({message: err});
    }
}

module.exports = allUserType
