const onlyCustomer = require("./onlyCustomer")
const onlySeller = require("./onlySeller")
const allUserType = require("./allUserType")
const paginationMiddleware = require("./paginationMiddleware")
module.exports = {
    onlyCustomer,
    onlySeller,
    allUserType,
    paginationMiddleware
}
