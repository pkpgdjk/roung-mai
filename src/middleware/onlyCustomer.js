var Token = require('../models/TokenModel');
var {JWT_KEY} = require('../../config');
const {USER_TYPE} = require('../constants');
var jwt = require('jsonwebtoken');

const onlyCustomer = async (req,res,next)=>{
    const t = req.headers["authorization"];
    let token = "";
    if (t && t.split(" ")[1]){
        token = t.split(" ")[1]
    }else{
        res.status(401).json({message:"คุณยังไม่ได้เข้าสู้ระบบ"});
        return;
    }

    const match = await Token.findOne({token});
    if (match){
        try {
            let decoded = jwt.verify(token, JWT_KEY);
            if (decoded.user.userType !== USER_TYPE.CUSTOMER){

                res.status(401).json({message:"คุณไม่ได้รับอณุญาติให้เข้าถึงส่วนของ customer"});
                return;
            }
            req['auth_user'] = decoded.user;
            req['token'] = token;
            next()
        } catch(err) {
            res.status(401).json({message:err});
        }
    }else{
        res.status(401).json({message:"คุณยังไม่ได้เข้าสู้ระบบ"});
    }
}

module.exports = onlyCustomer
