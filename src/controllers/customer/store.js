const express = require('express');
const User = require('../../models/UserModel');
const Seller = require('../../models/SellerModel');
const Menu = require('../../models/MenuModel');
const {USER_TYPE} = require('../../constants');
const {onlyCustomer,paginationMiddleware} = require('../../middleware');
const router = express.Router();
const { check,param, validationResult } = require('express-validator');
const isEmpty = require('lodash/isEmpty');
const utils = require("../../utils");

router.get('/', onlyCustomer, paginationMiddleware, async function (req, res, next) {
    const {perPage, page} = req.query;
    try {
        const menus = await User.find({userType:USER_TYPE.SELLER})
            .populate('seller',['storeName','description','imageFile'])
            .select("-password")
            .limit(perPage).skip(perPage * (page - 1)).lean()
            .exec();
        const count = await Seller.countDocuments({});
        const totalPage = Math.ceil(count / perPage);
        await res.json({
            data: menus,
            pagination: {
                page,
                perPage,
                totalPage
            }
        });
    } catch (e) {
        console.log(e);
        res.status(400).json({message: e});
    }
});


router.get('/menu/:sellerId', paginationMiddleware, [
    param('sellerId').exists().withMessage("กรุณากรอก seller id")
], async function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    const {perPage, page} = req.query;
    const {sellerId} = req.params;
    try {
        const userSeller = await User.findOne({_id: sellerId})
            .populate('seller',['storeName','description','imageFile'])
            .select('-password')
            .lean().exec();
        if (!userSeller) throw "ไม่พบ ร้านค้านี้ในระบบ";

        const menus = await Menu.find({seller:sellerId}).limit(perPage).skip(perPage * (page - 1)).lean().exec();
        const count = await Menu.countDocuments({seller:sellerId});
        const totalPage = Math.ceil(count / perPage);
        await res.json({
            data: menus,
            store:userSeller,
            pagination: {
                page,
                perPage,
                totalPage
            }
        });
    } catch (e) {
        console.log(e);
        res.status(400).json({message: e});
    }
});


module.exports = router;
