const express = require('express');
const User = require('../../models/UserModel');
const Seller = require('../../models/SellerModel');
const Menu = require('../../models/MenuModel');
const Order = require('../../models/OrderModel');
const {USER_TYPE,ORDER_STATUS_TYPE} = require('../../constants');
const {onlyCustomer,paginationMiddleware} = require('../../middleware');
const router = express.Router();
const { check,param, validationResult } = require('express-validator');
const isEmpty = require('lodash/isEmpty');
const utils = require("../../utils");
const {CANCELABLE} = require("../../../config");
const moment = require("moment");


router.get('/detail/:id', [
    param('id').exists().withMessage("กรุณาใส่ menu id")
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    const id = req.params.id;


    try {
        const order = await Order.findOne({_id:id,customer:req.auth_user._id})
            .select('-customer')
            .populate({
                path: "menu",
                model: "Menu",
                populate: {
                    path: "seller",
                    select: ["seller", "-_id"],
                    model: "User",
                    populate: {
                        path: "seller",
                        select: ["-money", "-idCard"],
                        model: "Seller"
                    }
                }
            })
            .lean().exec();

        if (!order) throw "ไม่พบ ออเดอร์นี้ในระบบ";

        // const addMoreDetail = order.map((item, i) => {
        //     let queue = (perPage * (page - 1)) + (i + 1);
        //     let o = {
        //         ...item,
        //         menu:{
        //             ...item.menu,
        //             store:item.menu.seller.seller,
        //             seller:undefined,
        //         },
        //         date: utils.moment(item.createdAt).format("DD/MM/YYYY HH:MM")
        //     };
        //     if (status === ORDER_STATUS_TYPE.COOKING) {
        //         o["queue"] = queue;
        //         o["cancelable"] = queue > CANCELABLE;
        //     }
        //     return o;
        // });

        order['menu']['store'] = order.menu.seller.seller
        order['date'] = utils.relativeTime(order.createdAt)
        // order['date'] = utils.moment(order.createdAt).format("DD/MM/YYYY HH:mm")
        if (order.status === ORDER_STATUS_TYPE.COOKING){
            let q = await utils.getQfromOrder(order);
            order["queue"] = q;
            order["cancelable"] = q > CANCELABLE;
        }

        delete order['menu']['seller'];
        return await res.json({
            data: order
        })
    }catch (e) {
        console.log(e);
        res.status(400).json({message: e});
    }
});


// type can be finish, cooking, canceled
router.get('/:type', paginationMiddleware,[
    param('type').exists().withMessage("กรุณาใส่ชนิดของ ออร์เดอร์")
], async function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    let status;

    if (Object.keys(ORDER_STATUS_TYPE).includes( (req.params.type || "").toUpperCase() )) {
        status = req.params.type.toUpperCase();
    }else{
        return res.status(400).json({
            message: `ชนิดของ ออร์เดอร์ มีแค่ [ ${Object.keys(ORDER_STATUS_TYPE).join()} ]`
        })
    }
    const user_id = req.auth_user._id;
    const {perPage, page} = req.query;
    try{
        let order = await Order.find({customer:user_id,status})
            .select('-customer')
            .populate({
                path:"menu",
                model:"Menu",
                populate:{
                    path:"seller",
                    select:["seller", "-_id"],
                    model:"User",
                    populate:{
                        path:"seller",
                        select:["-money","-idCard"],
                        model: "Seller"
                    }
                }
            })
            .limit(perPage).skip(perPage * (page - 1))
            .sort({createdAt: 'desc'})
            .lean().exec();
        const count = await Order.countDocuments({customer:user_id,status});
        const totalPage = Math.ceil(count / perPage);

        let addMoreDetail =[]
        order = order.filter(item => !!item.menu);
        for (let i=0;i<order.length;i++){
            let queue = (perPage * (page - 1)) + (i + 1);
            let o = {
                ...order[i],
                menu:{
                    ...order[i].menu,
                    store:order[i].menu.seller.seller,
                    seller:undefined,
                },
                // date: utils.moment(order[i].createdAt).format("DD/MM/YYYY HH:mm")
                date: utils.relativeTime(order[i].createdAt)
            };
            if (status === ORDER_STATUS_TYPE.COOKING) {
                o["queue"] = await utils.getQfromOrder(order[i]);
                o["cancelable"] = queue > CANCELABLE;
            }
            addMoreDetail.push(o);
        }

        await res.json({
            data: addMoreDetail,
            pagination: {
                page,
                perPage,
                totalPage
            }
        });
    }catch (e) {
        console.log(e);
        res.status(400).json({message: e});
    }

});

router.post('/', [
    check('menuId').exists().withMessage("กรุณาใส่ เมนู"),
    check('message').isLength({max: 500}).withMessage("ข้อความไม่ควรเกิน 500 ตัวอักษร"),
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    const user_id = req.auth_user._id;
    try {
        const menu = await Menu.findOne({_id: req.body.menuId});
        if (!menu) throw "ไม่พบ เมนูนี้ในระบบ";

        const order = new Order({
            seller:menu.seller,
            menu: req.body.menuId,
            message: req.body.message || "",
            price: menu.price,
            status:ORDER_STATUS_TYPE.COOKING,
            customer: user_id
        });
        await order.save();
        return await res.json({
            message:"สั่งอาหารเรียบร้อยแล้ว"
        })
    }catch (e) {
        console.log(e);
        res.status(400).json({message: e});
    }
});

router.put('/cancel/:id', [
    param('id').exists().withMessage("กรุณากรอก order id")
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    const user_id = req.auth_user._id;
    try {
        const order = await Order.findOne({_id: req.params.id,customer:user_id});
        if (!order) throw "ไม่พบ ออเดอร์นี้ในระบบ";

        const seller_orders = await Order.find({seller: order.seller, status: ORDER_STATUS_TYPE.COOKING})
            .limit(5)
            .sort({'createdAt': 'asc'})
            .lean()
            .exec();

        if (seller_orders.find(item => item._id.toString() === req.params.id)) {
            throw "คุณไม่สามารถยกเลิกเมนูได้ขณะ ที่ อยู่ในคิวต่ำกว่า 5";
        }

        order.status = ORDER_STATUS_TYPE.CANCELED;

        await order.save();
        return await res.json({
            message: "ยกเลิกออร์เดอร์นี้เรียบร้อยแล้ว"
        })
    }catch (e) {
        console.log(e);
        res.status(400).json({message: e});
    }
});

module.exports = router;
