const express = require('express');
const User = require('../../models/UserModel');
const Seller = require('../../models/SellerModel');
const Menu = require('../../models/MenuModel');
const {USER_TYPE} = require('../../constants');
const {onlyCustomer,paginationMiddleware} = require('../../middleware');
const router = express.Router();
const { check,param, validationResult } = require('express-validator');
const isEmpty = require('lodash/isEmpty');
const utils = require("../../utils");


router.get('/search',paginationMiddleware, async (req, res) => {
    const {perPage, page} = req.query;
    const search = req.query.menuName;
    if (!search) return await res.json({data: []})

    try{
        let menus = await Menu.find({menuName: new RegExp(search,"i") })
        .populate({
            path:"seller",
            populate: {
                path: "seller",
                select:['storeName','description']
            }
        })
            .limit(perPage).skip(perPage * (page - 1))
            .lean().exec();
        const count = await Menu.countDocuments({menuName: new RegExp(search,"i")});
        const totalPage = Math.ceil(count / perPage);

        menus = menus.map(item=>{
            let o = {
                ...item,
                store:item.seller.seller
            };
            delete o['seller'];
            return o
        })
        await res.json({
            data: menus,
            pagination: {
                page,
                perPage,
                totalPage
            }
        });
    }catch (e) {
        console.log(e);
        res.status(400).json({message: e});
    }
});
module.exports = router;



router.get('/:menuId', paginationMiddleware, [
    param('menuId').exists().withMessage("กรุณากรอก menu id")
], async function (req, res,) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    try{
        const menu = await Menu.findOne({_id: req.params.menuId}).lean().exec();
        if (!menu) throw "ไม่พบ เมนูนี้ในระบบ";

        await res.json({
            data: menu
        });
    }catch (e) {
        console.log(e);
        res.status(400).json({message: e});
    }
});

