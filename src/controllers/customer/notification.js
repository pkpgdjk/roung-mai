const express = require('express');
const User = require('../../models/UserModel');
const NotificationTokenModel = require('../../models/NotificationTokenModel');
const Seller = require('../../models/SellerModel');
const {check,validationResult} = require('express-validator');
const router = express.Router();
const resizebase64 = require('resize-base64');
const utils = require("../../utils");
const CONFIG = require("../../../config")


router.post('/', async function(req, res, next) {
    try{
        const notificationToken = req.body.token;
        const cusId = req.auth_user._id;
        let noti = await NotificationTokenModel.findOneAndUpdate({notificationToken}, {cusId, notificationToken});
        if (!noti){
            noti = new NotificationTokenModel({cusId, notificationToken});
            await noti.save();
        }
        return await res.json({
            status: true
        })


    }catch (e) {
        console.log(e);
        res.status(400).json({
            message: e
        })
    }
});


module.exports = router;
