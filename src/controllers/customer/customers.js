const express = require('express');
const User = require('../../models/UserModel');
const Customer = require('../../models/CustomerModel');
const Token = require('../../models/TokenModel');
const {USER_TYPE} = require('../../constants');
const {check,validationResult} = require('express-validator');
const {JWT_KEY} = require('../../../config');
const jwt = require('jsonwebtoken')
const {onlyCustomer} = require('../../middleware');
const menuRouter = require('./menu');
const storeRouter = require('./store');
const orderRouter = require('./order');
const profileRouter = require('./profile');
const notificationRouter = require('./notification');

const router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.json('respond with a resource');
});

router.post('/register',[
    check('name').exists().withMessage("กรุณากรอก ชื่อ"),
    check('email').exists().withMessage("กรุณากรอก email")
        .isEmail().withMessage("email ไม่ถูกต้อง")
        .custom(value => {
            return User.findOne({email:value}).then(user => {
                if (user) {
                    return Promise.reject('email นี้ มีอยู่ในระบบแล้ว');
                }
            })}
        ).withMessage("email นี้ มีอยู่ในระบบแล้ว")
    ,
    check('password').exists().withMessage("กรุณากรอก password"),
    check('confirmPassword').custom((value, { req }) => {
        console.log(value, req.body.password);
        if (value !== req.body.password) {
            throw new Error('รหัสผ่านไม่ตรงกัน');
        }else{
            return true;
        }
    }).withMessage("รหัสผ่านไม่ตรงกัน"),
    check('cardName').exists().withMessage("กรุณากรอก ชื่อที่แสดงบนบัตร"),
    check('cardNumber').exists().withMessage("กรุณากรอก เลขบัตรเครดิต")
        .isNumeric().withMessage("เลขบัตร ควรมีแค่ตัวเลข")
        .isLength({min:16,max:16}).withMessage("เลขบัตรเครดิตควรมี 16 หลัก"),
    check('expireDate').exists().withMessage("กรุณากรอก วันหมดอายุบัตร")
        .matches(/^((0?[1-9])|(1[0-2]))\/[0-9]{2}$/).withMessage("วันหมดอายุไม่ถูกต้อง ตัวอย่าง 12/24")
    ,
    check('cvv').exists().withMessage("กรุณากรอก รหัสหลังบัตร"),

], async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    if (!/^[a-zA-Zก-๑ .]+$/.test(req.body.name)){
        return await res.status(400).json({message: "ชื่อต้องประกอบไปด้วย A-Z และ ก-ฮ"});
    }


    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;
    const confirmPassword = req.body.confirmPassword;
    const cardNumber = req.body.cardNumber;
    const cardName = req.body.cardName;
    const expireDate = req.body.expireDate;
    const cvv = req.body.cvv;
    const imageFile = req.body.imageFile || "https://www.shareicon.net/data/512x512/2017/01/06/868320_people_512x512.png";

    try {
        const card = await (new Customer({cardNumber, cardName, expireDate, cvv})).save();

        const user = new User({
            name,
            email,
            password,
            customer: card._id,
            userType: USER_TYPE.CUSTOMER,
            imageFile
        });
        const createdUser = await user.save();
        const newUser = await User.findOne({email: createdUser.email}).populate('Customer');

        //auto login after register
        const token = await newUser.getToken();
        await res.json({
            message: "สมัครสมาชิกเรียบร้อยแล้ว",
            data: {
                user:newUser,
                token: token
            },
        });
    } catch (e) {
        await res.status(400).json({message: e})
    }
});

router.use('/menu', onlyCustomer, menuRouter);
router.use('/store', onlyCustomer, storeRouter);
router.use('/order', onlyCustomer, orderRouter);
router.use('/profile', onlyCustomer, profileRouter);
router.use('/notification', onlyCustomer, notificationRouter);

module.exports = router;
