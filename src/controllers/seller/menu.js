const express = require('express');
const User = require('../../models/UserModel');
const Seller = require('../../models/SellerModel');
const Menu = require('../../models/MenuModel');
const Order = require('../../models/OrderModel');
const {USER_TYPE} = require('../../constants');
const {onlySeller,paginationMiddleware} = require('../../middleware');
const router = express.Router();
const { check,param, validationResult } = require('express-validator');
const isEmpty = require('lodash/isEmpty');
const utils = require("../../utils");


router.get('/',onlySeller,paginationMiddleware, async function(req, res, next) {
    const {perPage, page} = req.query;
    try {
        const sellerId = req.auth_user._id;
        const menus = await Menu.find({seller: sellerId}).limit(perPage).skip(perPage * (page - 1)).lean().exec();
        const count = await Menu.countDocuments({seller:sellerId});
        const totalPage = Math.ceil(count / perPage);
        await res.json({
            data: menus,
            pagination: {
                page,
                perPage,
                totalPage
            }
        });
    }catch (e) {
        console.log(e);
        res.status(400).json({message: e});
    }
});

router.get('/detail/:menuId',onlySeller, paginationMiddleware, [
    param('menuId').exists().withMessage("กรุณากรอก menu id")
], async function (req, res,) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    const sellerId = req.auth_user._id;

    try{
        const menu = await Menu.findOne({_id: req.params.menuId,seller:sellerId}).lean().exec();
        if (!menu) throw "ไม่พบ เมนูนี้ในระบบ";

        await res.json({
            data: menu
        });
    }catch (e) {
        console.log(e);
        res.status(400).json({message: e});
    }
});

router.post('/', onlySeller, [
    check('menuName').exists().withMessage("กรุณากรอก ชื่อเมนู"),
    check('price').exists().withMessage("กรุณากรอก ราคา")
        .isInt({min:1}).withMessage("ราคาขั้นต่ำคือ 1 บาท"),
    check('imageFile').exists().withMessage("กรุณากรอก imageFile"),
], async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    try {
        const menuName = req.body.menuName;
        const description = req.body.description || "";
        const price = req.body.price;
        const seller = req.auth_user._id;
        let imageFile = "";
        if (utils.isURL(req.body.imageFile)){
            imageFile = req.body.imageFile
        }else{
            imageFile = await utils.resizeImage(req.body.imageFile,{width:400,height:300}) || "";
        }

        const menu = await (new Menu({menuName, description, price, imageFile, seller})).save();


        await res.json({
            message: "เพิ่มรายการเมนูเรียบร้อยแล้ว",
            data: menu
        });
    } catch (e) {
        res.status(400).json({message: e})
    }
});

router.put('/:id', onlySeller, [
    param('id').exists().withMessage('ไม่พบ เมนูนี้'),
    check('menuName').exists().withMessage("กรุณากรอก ชื่อเมนู"),
    check('price').exists().withMessage("กรุณากรอก ราคา")
        .isInt({min:1}).withMessage("ราคาขั้นต่ำคือ 1 บาท"),
    check('imageFile').exists().withMessage("กรุณากรอก imageFile"),
], async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    const id = req.params.id;
    const sellerId = req.auth_user._id;

    try{
        const menu = await Menu.findOne({_id:id,seller:sellerId});
       if (menu){
           menu.menuName = req.body.menuName;
           menu.price = req.body.price;
           if (!isEmpty(req.body.description)) menu.description = req.body.description;
           if (utils.isURL(req.body.imageFile)){
               menu.imageFile = req.body.imageFile
           }else{
               menu.imageFile = await utils.resizeImage(req.body.imageFile,{width:400,height:300}) || "";
           }
           menu.save();
           await res.json({
               message: "แก้ไขเมนูเรียบร้อยแล้ว",
               data: menu
           });
       }else{
           throw "ไม่พบผู้ใช้นี้ในระบบ";
       }
    }catch (e) {
        res.status(400).json({
            message: e
        })
    }

});

router.delete('/:id', onlySeller, [
    param('id').exists().withMessage("ไม่พบเมนูนี้ในระบบ")
], async (req, res) => {
    const id = req.params.id;
    const sellerId = req.auth_user._id;
    try{
        const menu = await Menu.deleteOne({_id:id,seller:sellerId});
        const order = await Order.deleteMany({menu: id});
        if (menu.deletedCount === 1) {
            res.json({
                message: "ลบเมนูเรียบร้อยแล้ว",
            });
        }else{
            throw "ไม่พบเมนูนี้ในระบบ";
        }
    }catch (e) {
        res.status(400).json({
            message: e
        })
    }
});

module.exports = router;
