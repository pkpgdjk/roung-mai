const express = require('express');
const User = require('../../models/UserModel');
const Withdraw = require('../../models/WithdrawModel');
const Seller = require('../../models/SellerModel');
const router = express.Router();
const {check,validationResult} = require('express-validator');
const {WITHDRAW_TYPE} = require('../../constants');
const isEmpty = require('lodash/isEmpty');
const utils = require("../../utils/index");
/* GET users listing. */
router.get('/', function(req, res, next) {
    res.json('respond with a resource');
});

router.post('/',[
    check('method').exists().withMessage("กรุณาเลือกประเภทการถอนเงิน"),
    check('promptpayAccount').custom((value, { req })=>{
        if (req.body.method === WITHDRAW_TYPE.PROMPTPAY){
            return !isEmpty(value) && /^[0-9]{10}$/.test(value);
        }
        return true;
    }).withMessage("เบอร์โทร ไม่ถูกต้อง"),
    check('bankType').custom((value, { req })=>{
        if (req.body.method === WITHDRAW_TYPE.BANK){
            return !isEmpty(value);
        }
        return true;
    }).withMessage("กรุณาเลือกประเภทธนาคาร"),
    check('bankNumber').custom((value, { req })=>{
        if (req.body.method === WITHDRAW_TYPE.BANK){
            return !isEmpty(value) && /^[0-9]{10}$/.test(value)
        }
        return true;
    }).withMessage("เลขบัญชีไม่ถูกต้อง"),
    check('amount').exists().withMessage("กรุณากรอกจำนวนเงิน").isInt({min:500}).withMessage("คุณสามารถถอนเงินได้ขั้นต่ำที่ 500 บาท"),
], async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    let withdraw;
    try{
        const seller = await Seller.findById(req.auth_user.seller._id);
        if (seller.money < req.body.amount){
            throw "คุณยอดเงินไม่เพียงพอสำหรับการถอน";
        }

        seller.money = seller.money - req.body.amount;
        await seller.save();

        if (req.body.method === WITHDRAW_TYPE.BANK ){
            withdraw = new Withdraw({
                storeId:req.auth_user._id,
                amount:req.body.amount,
                method:WITHDRAW_TYPE.BANK,
                bankType:req.body.bankType,
                bankNumber:req.body.bankNumber,
            })
        }else if (req.body.method === WITHDRAW_TYPE.PROMPTPAY){
            withdraw = new Withdraw({
                storeId:req.auth_user._id,
                amount:req.body.amount,
                method:WITHDRAW_TYPE.PROMPTPAY,
                promptpayAccount:req.body.promptpayAccount,
            })
        }else{
            throw "ประเภทการถอนเงินไม่ถูกต้อง";
        }
        await withdraw.save();

        await res.json({
            message: "ถอนเงินเรียบร้อยแล้ว",
            data:{
                transaction:{
                    date: utils.moment({}).format("DD MMMM YYYY - HH.MM"),
                    form:{
                        name:"Rongmai",
                        number:"xxxxxxxx123"
                    },
                    to:{
                        name: req.auth_user.name,
                        number : (withdraw.bankNumber || withdraw.promptpayAccount).replace(/^.{0,5}/,"xxxxx")
                    },
                    total: req.body.amount
                },
                seller: seller
            }
        })

    }catch (e) {
        console.log(e);
        await res.status(400).json({message:e})
    }
});


module.exports = router;
