const express = require('express');
const User = require('../../models/UserModel');
const Seller = require('../../models/SellerModel');
const {check,validationResult} = require('express-validator');
const CONFIG = require("../../../config")
const resizebase64 = require('resize-base64');
const router = express.Router();
const utils = require("../../utils");

/* GET users listing. */
router.get('/', async function(req, res, next) {
    try{
        const user = await User.findOne({_id:req.auth_user._id});
        res.json({
            data: user
        })
    }catch (e) {
        console.log(e);
        res.status(400).json({
            message: e
        })
    }
});

router.put('/',[
    check('name').exists().withMessage('กรุณากรอกชื่อ'),
    check('imageFile').exists().withMessage('กรุณาส่งรูปภาพ'),
    check('idCard').exists().withMessage('กรุณากรอก บัตรประชาชน')
        .isLength({min:13, max: 13}).withMessage("บัตรประชาชนต้องมี 13 ตัว")
        .isNumeric().withMessage("บัตรประชาชนต้องมี ควรมีแค่ตัวเลข")
    ,
],async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    if (!/^[a-zA-Zก-๑ .]+$/.test(req.body.name)){
        return await res.status(400).json({message: "ชื่อต้องประกอบไปด้วย A-Z และ ก-ฮ"});
    }

    try{

        const seller = await Seller.findOne({_id:req.auth_user.seller._id});
        seller.idCard = req.body.idCard;
        await seller.save();

        let user = await User.findOne({_id:req.auth_user._id});
        user.name = req.body.name;
        if (utils.isURL(req.body.imageFile)){
            user.imageFile = req.body.imageFile
        }else{
            user.imageFile = await utils.resizeImage(req.body.imageFile) || "";
        }
        user = await user.save();


        await res.json({
            message: "แก้ไขข้อมูลเรียบร้อยแล้ว",
            data: user
        });
    }catch (e) {
        console.log(e);
        await res.status(400).json({message:e})
    }
});


router.put('/store',[
    check('storeName').exists().withMessage('กรุณากรอกชื่อร้าน'),
],async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    try{

        const seller = await Seller.findOne({_id:req.auth_user.seller._id});
        seller.storeName = req.body.storeName;
        seller.description = req.body.description || "";
        if (utils.isURL(req.body.imageFile)){
            seller.imageFile = req.body.imageFile
        }else{
            seller.imageFile = await utils.resizeImage(req.body.imageFile,{width:400,height:300}) || "";
        }
        await seller.save();

        await res.json({
            message: "แก้ไขข้อมูลเรียบร้อยแล้ว",
            data:seller
        });
    }catch (e) {
        await res.status(400).json({message:e})
    }
});


router.put('/password',[
    check('oldPassword').exists().withMessage('กรุณากรอกรหัสผ่านเก่า'),
    check('newPassword').exists().withMessage('กรุณากรอกรหัสผ่านใหม่'),
    check('confirmPassword').custom((value, { req }) => {
        if (value !== req.body.newPassword) {
            throw new Error('รหัสผ่านไม่ตรงกัน');
        }else{
            return true;
        }
    }).withMessage("รหัสผ่านไม่ตรงกัน"),
],async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    const {
        newPassword,
        oldPassword
    } = req.body;

    try{
        const user = await User.findById(req.auth_user._id);
        user.comparePassword(oldPassword, async (err,isMatch) => {
            if (isMatch){
                user.password = newPassword;
                await user.save();
                await res.json({
                    message: "เปลี่ยนรหัสผ่านเรียบร้อยแล้ว",
                });
            }else {
                return  await res.status(400).json({message:"รหัสผ่านเดิมไม่ถูกต้อง"})
            }
        });
    }catch (e) {
        console.log(e)
        await res.status(400).json({message:e})
    }
});

module.exports = router;
