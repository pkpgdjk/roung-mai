const express = require('express');
const User = require('../../models/UserModel');
const Seller = require('../../models/SellerModel');
const Notification = require('../../models/NotificationModel');
const Menu = require('../../models/MenuModel');
const Order = require('../../models/OrderModel');
const {USER_TYPE,ORDER_STATUS_TYPE} = require('../../constants');
const {onlySeller,paginationMiddleware,onlyCustomer} = require('../../middleware');
const router = express.Router();
const { check,param,query, validationResult } = require('express-validator');
const isEmpty = require('lodash/isEmpty');
const utils = require("../../utils");
const {CANCELABLE} = require("../../../config");


router.get('/',paginationMiddleware, async function(req, res, next) {
    const {perPage, page} = req.query;
    try {
        const sellerId = req.auth_user._id;
        const menus = await Order.find({seller: sellerId, status: ORDER_STATUS_TYPE.COOKING})
            .populate({
                path: 'menu',
                select: ['menuName','description','imageFile','_id'],
                model: "Menu"
            })
            .select('-seller')
            .limit(perPage).skip(perPage * (page - 1))
            .sort({createdAt: 'asc'})
            .lean().exec();
        const count = await Order.countDocuments({seller: sellerId, status: ORDER_STATUS_TYPE.COOKING});
        const totalPage = Math.ceil(count / perPage);
        await res.json({
            data: menus.map(item=>({...item,date:utils.relativeTime(item.createdAt)})),
            pagination: {
                page,
                perPage,
                totalPage
            }
        });
    }catch (e) {
        console.log(e);
        res.status(400).json({message: e});
    }
});



router.get('/detail/:id', [
    param('id').exists().withMessage("กรุณาใส่ menu id")
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    const id = req.params.id;


    try {
        const order = await Order.findOne({_id:id,seller:req.auth_user._id})
            .select('-customer')
            .populate({
                path: "menu",
                model: "Menu",
            })
            .lean().exec();

        if (!order) throw "ไม่พบ ออเดอร์นี้ในระบบ";

        order['date'] = utils.relativeTime(order.createdAt)
        // order['date'] = utils.moment(order.createdAt).format("DD/MM/YYYY HH:mm")
        if (order.status === ORDER_STATUS_TYPE.COOKING){
            let q = await utils.getQfromOrder(order);
            order["queue"] = q;
            order["cancelable"] = q > CANCELABLE;
        }

        return await res.json({
            data: order
        })
    }catch (e) {
        console.log(e);
        res.status(400).json({message: e});
    }
});




router.get('/history',  paginationMiddleware, async (req, res) => {

    let date;
    if (req.query.date){
        date = {
            createdAt: {
                $gte: utils.moment(req.query.date).startOf('day').toDate(),
                $lte: utils.moment(req.query.date).endOf('day').toDate()
            }
        }
        console.log(date);
    }else {
        date = {}
    }

    const {perPage, page} = req.query;
    try {
        const sellerId = req.auth_user._id;
        const menus = await Order.find({
            seller: sellerId,
            status:ORDER_STATUS_TYPE.FINISH,
            ...date
        })
            .populate({
                path: 'menu',
                select: ['menuName', 'description', 'imageFile', '_id'],
                model: "Menu"
            })
            .select('-seller')
            .limit(perPage).skip(perPage * (page - 1))
            .sort({createdAt: 'desc'})
            .lean().exec();
        const count = await Order.countDocuments({
            seller: sellerId,
            ...date
        });
        const totalPage = Math.ceil(count / perPage);
        await res.json({
            data: menus.map(item => ({...item, date: utils.relativeTime(item.createdAt)})),
            // data: menus.map(item => ({...item, date: utils.moment(item.createdAt).format("DD/MM/YYYY HH:mm")})),
            pagination: {
                page,
                perPage,
                totalPage
            }
        });
    } catch (e) {
        console.log(e);
        res.status(400).json({message: e});
    }
});

router.put('/finish/:orderId', onlySeller, [
    param('orderId').exists().withMessage("กรุณากรอก Order id"),
], async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    try {

        const orderId = req.params.orderId;
        const sellerId = req.auth_user._id;
        const storeId = req.auth_user.seller._id;

        const order = await Order.findOne({_id: orderId, seller: sellerId})
            .populate({
                path: "seller",
                select: ["seller"],
                populate: {
                    path: "seller",
                    select: ["storeName", "description"],
                    model: "Seller",
                }
            })
            .populate({
                path: "menu",
                select: ["menuName", "description", "imageFile"]
            });
        // return res.json({ data: order });
        if (!order) throw "ไม่พบออเดอร์นี้ในระบบ";
        order.status = ORDER_STATUS_TYPE.FINISH;

        const seller = await Seller.findOne({_id: storeId});
        seller.money += order.price;

        await utils.pushNotification({
            cusId:order.customer,
            order: order
        })
        // const noti = new Notification({orderId, sellerId, cusId: order.customer});


        await order.save();
        await seller.save();
        // await noti.save();

        await res.json({
            message: "อัพเดทรายการออเดอร์เรียบร้อยแล้ว",
        });
    } catch (e) {
        console.log(e);
        res.status(400).json({message: e})
    }
});


module.exports = router;
