const express = require('express');
const User = require('../../models/UserModel');
const Seller = require('../../models/SellerModel');
const Token = require('../../models/TokenModel');
const {USER_TYPE} = require('../../constants');
const {JWT_KEY} = require('../../../config');
const menuRouter = require('./menu');
const profileRouter = require('./profile');
const withdrawRouter = require('./withdraw');
const orderRouter = require('./order');
const router = express.Router();
const {check,validationResult} = require('express-validator')
const jwt = require('jsonwebtoken')
const {onlySeller,onlyCustomer} = require('../../middleware')

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.json('respond with a resource');
});

router.post('/register',[
    check('name').exists().withMessage("กรุณากรอก ชื่อ"),
    check('email').exists().withMessage("กรุณากรอก email")
        .isEmail().withMessage("email ไม่ถูกต้อง")
        .custom(value => {
            return User.findOne({email:value}).then(user => {
                if (user) {
                    return Promise.reject('email นี้ มีอยู่ในระบบแล้ว');
                }
            })}
        ).withMessage("email นี้ มีอยู่ในระบบแล้ว")
    ,
    check('password').exists().withMessage("กรุณากรอก password"),
    check('storeName').exists().withMessage("กรุณากรอก ชื่อร้าน"),
    check('idCard').exists().withMessage("กรุณากรอก บัตรประชาชน")
        .isLength({min:13, max: 13}).withMessage("บัตรประชาชนต้องมี 13 ตัว")
        .isNumeric().withMessage("บัตรประชาชนต้องมี ควรมีแค่ตัวเลข"),
    check('confirmPassword').custom((value, { req }) => {
        console.log(value, req.body.password);
        if (value !== req.body.password) {
            throw new Error('รหัสผ่านไม่ตรงกัน');
        }else{
            return true;
        }
    }).withMessage("รหัสผ่านไม่ตรงกัน"),
] ,async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    if (!/^[a-zA-Zก-๑ .]+$/.test(req.body.name)){
        return await res.status(400).json({message: "ชื่อต้องประกอบไปด้วย A-Z และ ก-ฮ"});
    }

    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;
    const confirmPassword = req.body.confirmPassword;
    const storeName = req.body.storeName;
    const idCard = req.body.idCard;
    const description = req.body.description;
    const imageFile = req.body.imageFile || "https://www.shareicon.net/data/512x512/2017/01/06/868320_people_512x512.png";

    try {
        const card = await (new Seller({storeName, idCard, description, money: 0,imageFile:"https://via.placeholder.com/400x300?text=Rongmai"})).save();

        const user = new User({
            name,
            email,
            password,
            seller: card._id,
            userType: USER_TYPE.SELLER,
            imageFile
        });

        const createdUser = await user.save();
        const newUser = await User.findOne({email: createdUser.email}).populate('Seller');
        // console.log(newUser);

        //auto login after register
        const token = await newUser.getToken();
        await res.json({
            message: "สมัครสมาชิกเรียบร้อยแล้ว",
            data: {
                user:newUser,
                token: token
            },
        });
    } catch (e) {
        console.log(e);
        await res.status(400).json({message: e})
    }
});



router.use('/menu', menuRouter);
router.use('/profile', onlySeller,profileRouter);
router.use('/withdraw', onlySeller,withdrawRouter);
router.use('/order', onlySeller,orderRouter);

module.exports = router;
