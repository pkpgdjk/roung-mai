var express = require('express');
var User = require('../models/UserModel');
var Token = require('../models/TokenModel');
var NotificationToken = require('../models/NotificationTokenModel');
var router = express.Router();
var jwt = require('jsonwebtoken');
const {JWT_KEY} = require("../../config");
const {onlyCustomer,onlySeller,allUserType} = require("../middleware");


router.post('/login', async (req, res, next) => {
    const email = req.body.email
    const password = req.body.password
    if (!email && !password){
        res.status(400).json({message:"กรุณากรอก E-mail และ Password"})
    }else if (!email){
        res.status(400).json({message:"กรุณากรอก E-mail"})
    }else if (!password){
        res.status(400).json({message:"กรุณากรอก Password"})
    }


    try{
        const user = await User.findOne({email})
        if (user){
            user.comparePassword(password,async function(err, isMatch) {
                if (err) throw err;
                if (isMatch){
                    const token = await user.getToken();
                    res.json({
                        message: "เข้าสู่ระบบสำเร็จ",
                        userType: user.userType,
                        data: {token}
                    })
                }else{
                    res.status(400).json({message:"รหัสผ่านไม่ถูกต้อง"})
                }
            });
        }else{
            res.status(400).json({message:"ไม่พบผู้ใช้ในระบบ"})
        }
    }catch (error) {
        res.status(400).json({message:error})
    }
});

router.post('/logout', allUserType, async (req, res) => {
    try {
        const token = await Token.deleteMany({token: req.token});
        const notiToken = await NotificationToken.deleteMany({cusId: req.auth_user._id});
        if (token.deletedCount > 0) {
            return await res.json({
                message: "ออกจากระบบเรียบร้อยแล้ว"
            })
        } else {
            throw "ไม่พบ token นี้ในระบบ";
        }
    } catch (e) {
        console.log(e);
        return await res.status(400).json({message: e})
    }
});



module.exports = router;
