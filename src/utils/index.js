const m = require("moment-timezone");
const Order = require('../models/OrderModel');
const {USER_TYPE,ORDER_STATUS_TYPE} = require('../constants');
const sharp = require('sharp');
const CONFIG = require("../../config")
const pushNotification = require('./pushNotification');
require('moment/locale/th');
m.locale('th');

const deleteUndefinedKey = (obj) =>{
    Object.keys(obj).forEach(key => obj[key] === undefined && delete obj[key])
    return obj;
}

const moment = (t = "") => m(t).tz('Asia/Bangkok');


const getQfromOrder = async (order) =>{
    const c = await Order.find({
        "createdAt": {"$lte": order.createdAt},
        "status": ORDER_STATUS_TYPE.COOKING,
        "seller":order.seller
    }).count();
    console.log(order,c)
    return c;
}

const  isURL = (str) => {
    let pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
        '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
}


const resizeImage = async (base64,size = {width:CONFIG.IMAGE_SIZE.WIDTH,height:CONFIG.IMAGE_SIZE.HEIGHT}) =>{
    let parts = base64.split(';');
    let mimType = parts[0].split(':')[1];
    let imageData = parts[1].split(',')[1];

    var img = new Buffer(imageData, 'base64');
    const resizedImageBuffer = await sharp(img).resize(size.width,size.height).toBuffer()
    let resizedImageData = resizedImageBuffer.toString('base64');
    let resizedBase64 = `data:${mimType};base64,${resizedImageData}`;
    return resizedBase64;
}

const relativeTime = (date)=>{
    let now = moment({});
    let time = moment(date);
    if (time > now.subtract(1, "hours")) {
        return time.fromNow();
    }else{
        return time.format('DD/MM/YYYY HH:mm');
    }
}

module.exports = {
    deleteUndefinedKey,
    moment,
    getQfromOrder,
    isURL,
    resizeImage,
    relativeTime,
    pushNotification
}
