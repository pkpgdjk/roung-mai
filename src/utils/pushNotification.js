const NotificationTokenModel = require('../models/NotificationTokenModel');
const { Expo } = require('expo-server-sdk')
let expo = new Expo();

module.exports = async ({cusId,order})=>{
    // console.log(order);
    let notiToken = [];
    try{
        notiToken = await NotificationTokenModel.find({cusId}).lean().exec();
    }catch (e) {
        return [null, e];
    }

    let messages = [];
    for (let pushToken of notiToken) {
        if (!Expo.isExpoPushToken(pushToken.notificationToken)) {
            // return [null,{message: "notificationToken is not a valid"}]
            console.error(`Push token ${pushToken.notificationToken} is not a valid Expo push token`);
            continue;
        }
        // Construct a message (see https://docs.expo.io/versions/latest/guides/push-notifications.html)
        messages.push({
            to: pushToken.notificationToken,
            sound: 'default',
            title:`${order.menu.menuName} ของคุณเสร็จแล้ว`,
            body: `ไปรับอาหารของคุณได้ที่ ร้าน ${order.seller.seller.storeName}`,
            subtitle: `ไปรับอาหารของคุณได้ที่ ร้าน ${order.seller.seller.storeName}`,
            data: { orderId: order._id },
            priority:"high"
        })
    }

    let chunks = expo.chunkPushNotifications(messages);
    let tickets = [];
    // (async () => {
        // Send the chunks to the Expo push notification service. There are
        // different strategies you could use. A simple one is to send one chunk at a
        // time, which nicely spreads the load out over time:
        for (let chunk of chunks) {
            try {
                let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
                console.log(ticketChunk);
                tickets.push(...ticketChunk);
                // NOTE: If a ticket contains an error code in ticket.details.error, you
                // must handle it appropriately. The error codes are listed in the Expo
                // documentation:
                // https://docs.expo.io/versions/latest/guides/push-notifications#response-format
            } catch (error) {
                console.error(error);
            }
        }
    // })();

    let receiptIds = [];
    for (let ticket of tickets) {
        // NOTE: Not all tickets have IDs; for example, tickets for notifications
        // that could not be enqueued will have error information and no receipt ID.
        if (ticket.id) {
            receiptIds.push(ticket.id);
        }
    }

    let receiptIdChunks = expo.chunkPushNotificationReceiptIds(receiptIds);
    // (async () => {
        // Like sending notifications, there are different strategies you could use
        // to retrieve batches of receipts from the Expo service.
        for (let chunk of receiptIdChunks) {
            try {
                let receipts = await expo.getPushNotificationReceiptsAsync(chunk);
                console.log(receipts);

                // The receipts specify whether Apple or Google successfully received the
                // notification and information about an error, if one occurred.
                for (let receipt of receipts) {
                    if (receipt.status === 'ok') {
                        continue;
                    } else if (receipt.status === 'error') {
                        console.error(`There was an error sending a notification: ${receipt.message}`);
                        if (receipt.details && receipt.details.error) {
                            // The error codes are listed in the Expo documentation:
                            // https://docs.expo.io/versions/latest/guides/push-notifications#response-format
                            // You must handle the errors appropriately.
                            console.error(`The error code is ${receipt.details.error}`);
                        }
                    }
                }
                console.log(e);
            } catch (e) {
                console.log(e);
            }
        }
    // })();

    return [true, {}];
}
