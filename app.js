var createError = require('http-errors');
var express = require('express');
var path = require('path');
var logger = require('morgan');

require('dotenv').config()

var indexRouter = require('./src/controllers/index');
var customersRouter = require('./src/controllers/customer/customers');
var sellersRouter = require('./src/controllers/seller/sellers');

var app = express();



app.use(logger('dev'));
app.use(express.json({limit: '10mb'}));
app.use(express.urlencoded({ limit: '10mb',extended: false }));

app.use('/', indexRouter);
app.use('/customer', customersRouter);
app.use('/seller', sellersRouter);


//connect database
// console.log("connect to " + process.env.MONGO_URL)
// mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true })
//     .then(()=>{
//         console.log("Database connected")
//     })
//     .catch(error => ()=>{
//         console.log("DB_CONNECT_ERRRO::" + error)
//     });

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json('error');
});

module.exports = app;
